/**
 * @format
 * @NApiVersion 2.1
 */

define(["N/search", "N/record"], (searchApi, recordApi) => {
	return class BaseRecord {
		/**
		 * @param {N/record | integer}
		 * @param {string}
		 */
		constructor(record, type) {
			this.recordType = type;

			if (record.id) {
				this.id = record.id;
				this.record = record;
			} else {
				this.id = record;
				this.record = recordApi.create();
			}
		}

		/**
		 * @param {string} field
		 * @return {mixed}
		 */
		get(field) {
			return this.record.get(field);
		}

		/**
		 * @param {string} field
		 * @param {mixed} value
		 */
		set(field, value) {
			this.record.set(field, value);
		}

		/**
		 * @param {Object} keyValues
		 * @return {BaseRecord}
		 */
		hydrate(keyValues) {
			for (i = 0; i < Object.keys(keyValues).length; i++) {
				const field = Object.keys(keyValues)[i];
				const value = keyValues[field];
				this.record.set(field, value);
			}
		}
	};
});
