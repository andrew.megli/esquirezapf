/**
 * @format
 * @NApiVersion 2.1
 */

define([
	"N/query",
	"/SuiteScripts/record/BaseRecord",
	"/SuiteScripts/record/ContactRecord",
], (queryApi, BaseRecord, ContactRecord) => {
	return class ClientRecord extends BaseRecord {
		constructor(record) {
			super(record, "client");
		}

		/**
		 * Find all attorneys related to this client
		 * @return {ContactRecord[]}
		 */
		getAttorneys() {
			const sql = {
				query: `
						SELECT * 
						FROM ${ContactRecord.type}
						WHERE ${ContactRecord.RECORD_TYPE}.client_id = ${this.id}
				`,
			};

			const attorneys = queryApi
				.runSuiteQL(sql)
				.asMappedResults()
				.map((result) => new ContactRecord(result.id).hydrate(result));

			return attorneys;
		}

		/**
		 * Find all child clients (if any) related to this client
		 * @return {ClientRecord[]}
		 */
		getChildClients() {
			const sql = `
				SELECT id FROM ${this.RECORD_TYPE}
				WHERE parent = ${this.id} AND isinactive = 'F'
			`;
			return queryApi
				.runSuiteQL({
					query: sql,
				})
				.asMappedResults()
				.map((result) => new ClientRecord(result.id));
		}
	};
});
